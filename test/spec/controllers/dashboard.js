'use strict';

describe('DashboardController', function() {
  beforeEach(module('projectTodoList'));

  var $controller;

  beforeEach(inject(function(_$controller_){
    // The injector unwraps the underscores (_) from around the parameter names when matching
    $controller = _$controller_;
  }));

  describe('$scope.projects', function() {
    it('get the list of projects', function() {
      var $scope = {};
      var controller = $controller('DashboardController', { $scope: $scope });
      expect($scope.projects).toBeGreaterThan(0);
    });
  });
});
