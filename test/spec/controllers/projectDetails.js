'use strict';

describe('ProjectDetailsController', function() {
  beforeEach(module('projectTodoList'));

  var $controller;

  beforeEach(inject(function(_$controller_){
    // The injector unwraps the underscores (_) from around the parameter names when matching
    $controller = _$controller_;
  }));

  describe('$scope.projects', function() {
    it('get the project details', function() {
      var $scope = {};
      //var controller = $controller('ProjectDetailsController', { $scope: $scope });
      expect(projectDetails(1)).length(1);
    });
  });
});
