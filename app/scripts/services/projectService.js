'use strict';
angular.module('projectTodoList')
.service('projectService',function(){
    this.getProjects = function(){
        return [
            { id: 1, name: 'Test1', status: 'Done'},
            { id: 2, name: 'Test2', status: 'Done'},
            { id: 3, name: 'Test3', status: 'Pending'},
            { id: 4, name: 'Test4', status: 'Done'},
            { id: 5, name: 'Test5', status: 'Pending'},
            { id: 6, name: 'Test6', status: 'Done'},
            { id: 7, name: 'Test7', status: 'Pending'},
            { id: 8, name: 'Test8', status: 'Done'},
            { id: 9, name: 'Test9', status: 'Done'},
            { id: 10, name: 'Test10', status: 'Pending'}
          ];
    };
});