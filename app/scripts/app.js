'use strict';

/**
 * @ngdoc overview
 * @name projectTodoList
 * @description
 * # projectTodoList
 *
 * Main module of the application.
 */
angular
  .module('projectTodoList', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ui.router',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function($stateProvider, $urlRouterProvider) {
    $stateProvider
    .state('dashboard', {
        abstract: true,
        template: '<nav ui-view="header" class="navbar navbar-inverse navbar-fixed-top"></nav>'+
                      '<div class="container-fluid"> <div class="row"> <div ui-view="left-sidebar" class="col-sm-3 col-md-2 sidebar"></div>'+
                      '<div ui-view="dashboard" class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main"></div> </div>'+
                      '<div ui-view="footer"></div> </div>',
    })
    .state('dashboard.physical',{
      url:'/dashboard',
      views: {
        'header':{
            templateUrl : 'views/header.html'
        },
        'left-sidebar' : {
            templateUrl : 'views/left-sidebar.html',
        },
        'dashboard' : {
            templateUrl : 'views/dashboard.html',
            controller : 'DashBoardController'
        },
        'footer' : {
            templateUrl : 'views/footer.html'
        }
      }
    })
    .state('project-details', {
        abstract: true,
        template: '<nav ui-view="header" class="navbar navbar-inverse navbar-fixed-top"></nav>'+
                      '<div class="container-fluid"> <div class="row"> <div ui-view="left-sidebar" class="col-sm-3 col-md-2 sidebar"></div>'+
                      '<div ui-view="project-details" class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main"></div> </div>'+
                      '<div ui-view="footer"></div> </div>',
    })
    .state('project-details.physical',{
      url:'/project-details/:id',
      views: {
        'header':{
            templateUrl : 'views/header.html'
        },
        'left-sidebar' : {
            templateUrl : 'views/left-sidebar.html',
        },
        'project-details' : {
            templateUrl : 'views/project-details.html',
            controller: 'ProjectDetailsController'
        },
        'footer' : {
            templateUrl : 'views/footer.html'
        }
      }
    });
    $urlRouterProvider.otherwise('/dashboard');
  }).run(function(){
    return true;
  });
