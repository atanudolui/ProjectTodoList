"use strict";
angular.module('projectTodoList')
.controller('ProjectDetailsController',['$scope','projectService','$stateParams',function($scope,projectService,$stateParams){
	var projects = function(){ 
		return projectService.getProjects();
	}
	var projectDetails = function(project_id){
		return projects().filter(function(project){
			return (project.id == project_id);
		});
	} 
	$scope.projectDetails = projectDetails($stateParams.id)[0];
}]);