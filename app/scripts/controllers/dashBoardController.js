"use strict";
angular.module('projectTodoList')
.controller('DashBoardController',['$scope','projectService',function($scope,projectService){
	$scope.searchProject   = '';
	$scope.projects = projectService.getProjects();
}]);