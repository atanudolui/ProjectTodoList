'use strict';

/**
 * @ngdoc function
 * @name projectTodoList.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the projectTodoList
 */
angular.module('projectTodoList')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
